#!/bin/zsh

setopt shwordsplit

testSetWrongConfigPath () {
    $mpia -c /somenonexistingpath >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertFalse 'expected output on STDERR, but got none' "[ ! -s '${stderrF}' ]"
    assertEquals 1 $rtrn
}

testAssembleDataflowFromConfig () {
    $mpia -n -c $config -d /Tsource -r $readme >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    expStdout='Dataflow Overview:
  <SOURCE> /Tsource
  rsync-> <STAGING> Tstagingpath/Tsource-57b691c69c7cb24cff13bf2d385be202a28eb9d3
    tar-> <ARCHIVE> Tarchivepath/Tsource-57b691c69c7cb24cff13bf2d385be202a28eb9d3-DATA.tar
  rsync-> <TARGET>  Thost:Tpath/Tdept'
    assertEquals 'unexpected output' "$expStdout" "$(cat $stdoutF)"
}

testArchiveSourceSyntax1 () {
    $mpia -n -c $config -d /Tsource -r $readme >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    assertFalse 'unexpected output to STDERR' "[ -s '$stderrF' ]"
}

testArchiveSourceSyntax2 () {
    $mpia -n -c $config -d Tsource -r $readme >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 1 $rtrn
    assertTrue 'missing output to STDERR' "[ -s '$stderrF' ]"
}

testArchiveSourceSyntax3 () {
    $mpia -n -c $config -d Host:/Tsource -r $readme >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    assertFalse 'unexpected output to STDERR' "[ -s '$stderrF' ]"
}

testArchiveSourceSyntax4 () {
    $mpia -n -c $config -d Host:Tsource -r $readme >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 1 $rtrn
    assertTrue 'missing output to STDERR' "[ -s '$stderrF' ]"
}

testTag () {
    local tag="my_tag,V2"
    $mpia -n -c $config -d /Tsource -r $readme -t $tag >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    assertFalse 'unexpected output to STDERR' "[ -s '$stderrF' ]"
    assertEquals 'unexpected output' "    tar-> <ARCHIVE> Tarchivepath/Tsource-57b691c69c7cb24cff13bf2d385be202a28eb9d3-${tag}-DATA.tar" "$(grep ${tag}-DATA $stdoutF)"
}

testTagIllegal () {
    $mpia -n -c $config -d /Tsource -r $readme -t "myTag-v2" >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 1 $rtrn
    assertTrue 'missing output to STDERR' "[ -s '$stderrF' ]"
}

oneTimeSetUp()
{
    outputDir="${SHUNIT_TMPDIR}/output"
    mkdir -p $outputDir
    stdoutF="${outputDir}/stdout"
    stderrF="${outputDir}/stderr"
    config=$SHUNIT_TMPDIR/config
    cat >$config <<EOF
MPIA_HOST=Thost:
MPIA_PATH=Tpath
MPIA_DEPT=Tdept
MPIA_PREFLIGHT=true
MPIA_STAGING_PATH=Tstagingpath
MPIA_ARCHIVE_PATH=Tarchivepath
EOF
    readme=$SHUNIT_TMPDIR/readme
    touch $readme
    mpia="../mpi-archiver"
}

# load and run shUnit2
[ -n "${ZSH_VERSION:-}" ] && SHUNIT_PARENT=$0
. ../shunit2/2.1/src/shunit2
