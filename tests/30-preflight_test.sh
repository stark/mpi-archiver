#!/bin/zsh

setopt shwordsplit

testFailingPreflight () {
    $mpia -c $config -r $readme -d /asdf >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 1 $rtrn
    assertEquals 'expected other stderr' 'mpi-archiver: preflight script returned 1, exiting.' "$(cat $stderrF)"
}

oneTimeSetUp()
{
    outputDir="${SHUNIT_TMPDIR}/output"
    mkdir -p $outputDir
    stdoutF="${outputDir}/stdout"
    stderrF="${outputDir}/stderr"
    config=$SHUNIT_TMPDIR/config
    cat >$config <<EOF
MPIA_PREFLIGHT=false
MPIA_HOST=
EOF
    readme=$SHUNIT_TMPDIR/readme
    touch $readme
    mpia="../mpi-archiver"
}

# load and run shUnit2
[ -n "${ZSH_VERSION:-}" ] && SHUNIT_PARENT=$0
. ../shunit2/2.1/src/shunit2
