#!/bin/zsh

setopt shwordsplit
zmodload zsh/stat

testArchive () {
    $mpia -c $config -r $readme -d $data >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    assertTrue 'missing archive file' "[ -e '$SHUNIT_TMPDIR/archive/${aname}-DATA.tar' ]"
}

testArchiveContents () {
    tar tf $SHUNIT_TMPDIR/archive/${aname}-DATA.tar >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    assertEquals 'unexpected archive contents' "$(sed s/data-a74951564ed413dbffdafd23b399fc6467f738a6/${aname}/ ${SHUNIT_PARENT%.sh}.data)" "$(sort $stdoutF)"
}

testArchiveExtract () {
    unpackdir=$SHUNIT_TMPDIR/unpack
    mkdir $unpackdir
    tar xf $SHUNIT_TMPDIR/archive/${aname}-DATA.tar -C $unpackdir ${aname}/data/b/2
    rtrn=$?
    assertEquals 0 $rtrn
    assertTrue 'could not extract file from archive' "[ -e '$unpackdir/${aname}/data/b/2' ]"
}

oneTimeSetUp () {
    outputDir="${SHUNIT_TMPDIR}/output"
    mkdir -p $outputDir
    stdoutF="${outputDir}/stdout"
    stderrF="${outputDir}/stderr"
    config=$SHUNIT_TMPDIR/config
    cat >$config <<EOF
MPIA_HOST=""
MPIA_PATH=$SHUNIT_TMPDIR/target
MPIA_PREFLIGHT=true
MPIA_STAGING_PATH=$SHUNIT_TMPDIR/staging
MPIA_ARCHIVE_PATH=$SHUNIT_TMPDIR/archive
EOF
    mkdir $SHUNIT_TMPDIR/staging
    mkdir $SHUNIT_TMPDIR/archive
    mkdir $SHUNIT_TMPDIR/target
    readme=$SHUNIT_TMPDIR/readme
    touch $readme
    data=$SHUNIT_TMPDIR/data
    aname=${data:t}-"$(print -- $data | sha1sum | awk '{print $1}')"
    mkdir $data
    for d in a b c d e f g
    do
        mkdir $data/$d
        for f in 1 2 3 4 5 6
        do
            touch $data/$d/$f
        done
    done
    mpia="../mpi-archiver"
}

oneTimeTearDown () {
    rm -rf ${SHUNIT_TMPDIR}
}

# load and run shUnit2
[ -n "${ZSH_VERSION:-}" ] && SHUNIT_PARENT=$0
. ../shunit2/2.1/src/shunit2
