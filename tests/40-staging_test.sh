#!/bin/zsh

setopt shwordsplit

testCopyToStaging () {
    $mpia -c $config -r $readme -d $data >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 0 $rtrn
    assertTrue 'missing staging stamp' "[ -e '$SHUNIT_TMPDIR/staging/.${aname}-staging_done' ]"
}

testCopyToStagingRsyncFail () {
    # invalid rsync option simulating rsync failure
    cat >>$config <<EOF
MPIA_RSYNC_OPTS_STAGING="-Y"
EOF
    $mpia -c $config -r $readme -d $data >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 1 $rtrn
    assertFalse 'staging stamp should not be here' "[ -e '$SHUNIT_TMPDIR/staging/.staging_done' ]"
}

setUp () {
    outputDir="${SHUNIT_TMPDIR}/output"
    mkdir -p $outputDir
    stdoutF="${outputDir}/stdout"
    stderrF="${outputDir}/stderr"
    config=$SHUNIT_TMPDIR/config
    cat >$config <<EOF
MPIA_HOST=""
MPIA_PATH=$SHUNIT_TMPDIR/target
MPIA_PREFLIGHT=true
MPIA_STAGING_PATH=$SHUNIT_TMPDIR/staging
MPIA_ARCHIVE_PATH=$SHUNIT_TMPDIR/archive
EOF
    mkdir $SHUNIT_TMPDIR/staging
    mkdir $SHUNIT_TMPDIR/archive
    mkdir $SHUNIT_TMPDIR/target
    readme=$SHUNIT_TMPDIR/readme
    touch $readme
    data=$SHUNIT_TMPDIR/data
    aname=${data:t}-"$(print -- $data | sha1sum | awk '{print $1}')"
    mkdir $data
    for d in a b c d e f g
    do
        mkdir $data/$d
        for f in 1 2 3 4 5 6
        do
            touch $data/$d/$f
        done
    done
    mpia="../mpi-archiver"
}

tearDown () {
    rm -rf ${SHUNIT_TMPDIR}
}

# load and run shUnit2
[ -n "${ZSH_VERSION:-}" ] && SHUNIT_PARENT=$0
. ../shunit2/2.1/src/shunit2
