#!/bin/zsh

setopt shwordsplit

testPrintHelp () {
    $mpia -h >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    th_assertTrueWithStdOutput $rtrn "${stdoutF}" "${stderrF}"
}

testPrintVersion () {
    $mpia -v >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    th_assertTrueWithStdOutput ${rtrn} "${stdoutF}" "${stderrF}"
}

testNoParams () {
    $mpia >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertFalse 'expected output on STDERR, but got none' "[ ! -s '${stderrF}' ]"
    assertEquals 1 $rtrn
}

testWrongParam () {
    $mpia -x >"$stdoutF" 2>"$stderrF"
    rtrn=$?
    assertEquals 1 $rtrn
    expErr='mpi-archiver: unexpected argument: -x'
    assertEquals "$expErr" "$(cat $stderrF)"
}

th_assertTrueWithStdOutput()
{
    th_return_=$1
    th_stdout_=$2
    th_stderr_=$3

    assertFalse 'expected output on STDOUT, but got none' "[ ! -s '${th_stdout_}' ]"
    assertFalse 'unexpected output to STDERR' "[ -s '${th_stderr_}' ]"
    assertEquals 0 $th_return_

    unset th_return_ th_stdout_ th_stderr_
}

oneTimeSetUp()
{
    outputDir="${SHUNIT_TMPDIR}/output"
    mkdir -p $outputDir
    stdoutF="${outputDir}/stdout"
    stderrF="${outputDir}/stderr"
    mpia="../mpi-archiver"
}

tearDown()
{
    rm -fr "${testDir}"
}

# load and run shUnit2
[ -n "${ZSH_VERSION:-}" ] && SHUNIT_PARENT=$0
. ../shunit2/2.1/src/shunit2
